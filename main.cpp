#include <string.h>
#include <fstream>
#include <iostream>
#include <cmath>
#include <stdio.h>
#define file "file.bin"

using namespace std;
typedef struct TABLE		// îãîëîøåííÿ ñòðóêòóðè
{
	char NAZV[15];
	int NUMR;
	char DATE[10];
	char TIME[5];
}tab;
// îãîëîøåííÿ ôóíêö³é
void write_file(tab arr);
void show_info(tab arr);
void update(tab arr);
void clean();

int main()
{
	tab arr;	// åêçåìïëÿð ñðóêòóðè
	char i;
	// ìåíþ
do{
		cout << "1- enter the info ; update -2; 3- show information; 4- clean the file ;q- exit" << endl;
		cin >> i;
		 switch (i)
		 {
		 	case '1': 
				 {
					write_file(arr);
				break;
				 }
		 	case '2':
			 	{
					update(arr);
				break;
				}
		 	case '3':
			 	{
					show_info(arr);
				break;
				}	
		 	case '4':
			 	{
					clean();
				break;
				}			
		 	case 'q': return 0;
			default:	
			{
				cout << "incorrect input" << endl;
				break;
			}
		 }
	}while(i!='q');
	system("pause");
    return 0;
}
	// ôóíêö³ÿ çàïèñó äàíèõ â ôàéë
void write_file(tab arr)
{
	int i=0, q=0; // ë³÷èëüíèê
	cout << "create a new file" << endl;
	ofstream output_file(file, ios::binary);	// â³äêðèâàííÿ ôàéëó
	cout << "enter the number of trains" << endl; cin >> q;		// ê³ëüê³ñòü çàïèñ³â
	for (i=0;i<q;i++)
	{
		cout << "enter the destination" << endl;  cin >> arr.NAZV;
		cout << "enter the ID of the train" << endl; cin >> arr.NUMR;
		cout << "enter the date in format (XX.XX.XXXX)" << endl; cin >> arr.DATE;
		cout << "enter the time in format (xx:xx)" << endl; cin >> arr.TIME; 
		cin.ignore();
		if (cin.fail())  	// ïåðåâ³ðêà íà ïðïâèëüíèé ââ³ä
	{
		cout << "incorrect input" << endl;
		system("pause");
		return;
	}
    	output_file.write((char*)&arr, sizeof(arr));
	}
    output_file.close();
}

void show_info(tab arr)	// ôóíêö³ÿ âèâîäó âñ³º¿ ³íôîðìàö³¿
{
	cout << "[opening the file]" << endl;	
   	ifstream input_file(file, ios::binary);
   	if(!input_file) // ïåðåâ³ðêà íàÿâíîñò³ ôàéëó	
	{ 
		cout <<"error 404" << endl; 
	} 
	cout << "[reading info]" << endl;
	// ç÷èòóâàííÿ ôàéëó â ñòðóêòóðó
	tab mas[100]; 
	int n=0;
 	input_file.read((char*)&mas, sizeof(mas));
	cout << "[rows:]"<< n << endl;
	cout << "[info:]" << endl;
	cout << "enter the number of rows you want to show" << endl; cin >> n;	
		// âèâ³ä óñ³º¿ ³íôîðìàö³¿
	for (int i=0;i<n;i++)
	{
		cout << "Destination:" << endl; cout <<  mas[i].NAZV << endl;	
		cout << "ID of the train:" << endl; cout << mas[i].NUMR << endl;
		cout << "date:" << endl; cout << mas[i].DATE << endl;
		cout << "time:" << endl; cout << mas[i].TIME << endl << endl;
	}
	input_file.close();
}

void clean()	// ôóíêö³ÿ î÷èùåííÿ ôàéëó
{
	ofstream file_(file,ios::trunc); 
	file_.close(); 
	cout << "exit the program for successful erasing" << endl;
}

void update(tab arr)
{
	int i=0, q=0; // ë³÷èëüíèê
	cout << "create a new file" << endl;
	ofstream output_file(file, ios::app | ios::binary);	// â³äêðèâàííÿ ôàéëó
	cout << "enter the number of trains" << endl; cin >> q;		// ê³ëüê³ñòü çàïèñ³â
	for (i=0;i<q;i++)
	{
		cout << "enter the destination" << endl;  cin >> arr.NAZV;
		cout << "enter the ID of the train" << endl; cin >> arr.NUMR;
		cout << "enter the date in format (XX.XX.XXXX)" << endl; cin >> arr.DATE;
		cout << "enter the time in format (xx:xx)" << endl; cin >> arr.TIME; 
		cin.ignore();
		if (cin.fail())  	// ïåðåâ³ðêà íà ïðïâèëüíèé ââ³ä
	{
		cout << "incorrect input" << endl;
		system("pause");
		return;
	}
    	output_file.write((char*)&arr, sizeof(arr));
	}
    output_file.close();
}